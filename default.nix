{ pkgs ? import <nixpkgs> {} }:
{
  tally = pkgs.callPackage ./packages/tally.nix {};
}
