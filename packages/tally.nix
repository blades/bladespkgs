{ fetchFromGitHub, rustPlatform, stdenv, substituteAll }:

rustPlatform.buildRustPackage rec {
  pname = "tally";
  version = "0.4.2";

  src = fetchFromGitHub {
    owner = "jonhoo";
    repo = pname;
    rev = "c8e60e16e32a952421920b94deb05052c4077d17";
    sha256 = "1md0jfc5hw7f0jz6dbcbzppiq5clnxk05iag9qxyy7vhhn3s4105";
  };

  cargoSha256 = "14gnnmhnxqqy401y5y03s29nd82ap0q4lv1ii9gf7qmj3qydlzid";

  meta = with stdenv.lib; {
    description = "A prettier version of the time command";
    homepage = "https://github.com/jonhoo/tally/";
    license = with licenses; [ mit /* or */ asl20 ];
    platforms = platforms.all;
  };
}
