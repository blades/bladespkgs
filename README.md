# bladespkgs

A collection of nix pkgs that either aren't in nixpkgs, or I have a need of some modification.

## Usage

Example nix shell:

```nix
{ pkgs ? import <nixpkgs> {}}:

let
  bladespkgs = let
    src = pkgs.fetchFromGitLab {
      owner = "blades";
      repo = "bladespkgs";
      rev = "f1be0c17ec9dcc474fa0e81dd12cfd157d7631b6";
      sha256 = "03p2wsq9zrs45j9z63sp44hi86l7dwz4pmsianvbrymmpcm683br";
    };
  in import src { inherit pkgs; };
in pkgs.mkShell {
  buildInputs = [ bladespkgs.tally ];
}
```
